import json
import smtplib
import datetime
import codecs
import json
import sys, getopt
import redis
from urllib.parse import quote
from package.browser.Browser import Browser
from package.browser.BrowserInterface import BrowserInterface
from package.connector.Connector import Connector
from package.connector.ConnectorInterface import ConnectorInterface
from package.crawler.HtmlCrawler import HtmlCrawler
from package.crawler.HtmlCrawlerInterface import HtmlCrawlerInterface
from package.retailer.AbstractRetailer import AbstractRetailer 
from package.retailer.LdlcRetailer import LdlcRetailer
from package.retailer.MaterielnetRetailer import MaterielnetRetailer
from package.message.Email import Email
from package.storage.Redis import Redis

def get_cards_from_retailers():
  # 1°) Get retailers details JSON
  with open('/app/retailers.json') as retails_json:
    retailers_details = json.load(retails_json)

  # 2°) Get cards by retailers dictionary from NVidia API
  cards_by_retailers = get_cards_list()

  # 3°) Connect to each retailers to get cards availability and price
  cards_available_list   = {}
  cards_unavailable_list = {}
  for retailer_url in cards_by_retailers:
    if retailer_url in retailers_details:
      browser  = connect_to_retailer(retailers_details[retailer_url])
      for card_type_ref in cards_by_retailers[retailer_url]:
        retailer = search_product(retailer_url, browser, cards_by_retailers[retailer_url][card_type_ref], retailers_details[retailer_url])

        if retailer is not None:
          if (card_type_ref not in cards_available_list) and (card_type_ref not in cards_unavailable_list):
            cards_available_list[card_type_ref]   = retailer.get_products_available_list()
            cards_unavailable_list[card_type_ref] = retailer.get_products_unavailable_list()
          else:
            cards_available_list[card_type_ref]   = cards_available_list[card_type_ref] + retailer.get_products_available_list()
            cards_unavailable_list[card_type_ref] = cards_unavailable_list[card_type_ref] + retailer.get_products_unavailable_list()
      
      # Close Browser web driver after each loop
      browser.close_web_driver()

  return {"available": cards_available_list, "unavailable": cards_unavailable_list}

"""Connect to retailer web site"""
def connect_to_retailer(retailer_details: dict) -> BrowserInterface:
  if 'sign_in_url' in retailer_details:
    # Init Browser web driver
    browser    = Browser()
    connection = Connector(browser, retailer_details['login'], retailer_details['password'])
    connection.fill_in_login_form(retailer_details['sign_in_url'], retailer_details['sign_in_form_login_id'], retailer_details['sign_in_form_password_id'])
    connection.submit_login_form(retailer_details['sign_in_form_button_xpath'])
    return browser

""" Get all cards available on retailers from NVidia web site """ 
def get_cards_list() -> dict:
  # Opening JSON file
  cards_by_retailers = {}
  json_file          = open('/app/api.json')
  json_api           = json.load(json_file)
  crawler            = HtmlCrawler()

  for key in json_api["url_parameters"]:
    json_return = crawler.get(json_api['url'], json_api["url_parameters"][key]['url_parameters'])
    for productDetail in json_return['searchedProducts']['productDetails']:
      for retailer in productDetail['retailers']:
        if retailer['retailerName'] not in cards_by_retailers:
          cards_by_retailers[retailer['retailerName']] = {key: [retailer['productTitle']]}
        else:
          if key in cards_by_retailers[retailer['retailerName']]:
            if retailer['productTitle'] not in cards_by_retailers[retailer['retailerName']][key]:
              cards_by_retailers[retailer['retailerName']][key].append(retailer['productTitle'])
          else:
            cards_by_retailers[retailer['retailerName']][key] = [retailer['productTitle']]

  cards_by_retailers['https://www.materiel.net']['6900'] = ['RX 6900 XT']
  cards_by_retailers['https://www.ldlc.com']['6900']     = ['RX 6900 XT']
  cards_by_retailers['https://www.materiel.net']['1660'] = ['1660 super']
  cards_by_retailers['https://www.ldlc.com']['1660']     = ['1660 super']

  return cards_by_retailers

""" Search for product on retailer web site"""
def search_product(retailer_url, browser, retailer_cards, retailer_details):
  retailer = None
  for retailer_card_name in retailer_cards:
    html = browser.fill_form_and_submit(retailer_details['retailer_search_url'], [{"id": retailer_details['retailer_search_input_id'], "value": retailer_card_name}], retailer_details['retailer_search_button_xpath'])

    if retailer is None:
      if 'ldlc_com' == retailer_details['retailer_name']:
        retailer = LdlcRetailer()

      if 'materiel_net' == retailer_details['retailer_name']:
        retailer = MaterielnetRetailer()

    if retailer is not None:
      retailer.search_product(retailer_url, html)

  return retailer

def sumup():
  json_api = json.load(open('/app/config.json'))
  cards    = get_cards_from_retailers()

  if len(cards['available']) > 0 or len(cards['unavailable']) > 0:
    for email in json_api['email_list']:

      available_cards_watched_by_email = {} 
      unavailable_cards_watched_by_email = {}
      for card_ref in json_api['email_list'][email]:
        # Get cards available watched by user
        if card_ref in cards['available']:
          for card in cards['available'][card_ref]:
            if card['product_name'].find(card_ref) > 0:
              if card_ref not in available_cards_watched_by_email:
                available_cards_watched_by_email[card_ref] = [card]
              else:
                available_cards_watched_by_email[card_ref].append(card)

        # Get cards unavailable watched by user  
        if card_ref in cards['unavailable']:
          for card in cards['unavailable'][card_ref]:
            if card['product_name'].find(card_ref) > 0:
              if card_ref not in unavailable_cards_watched_by_email:
                unavailable_cards_watched_by_email[card_ref] = [card]
              else:
                unavailable_cards_watched_by_email[card_ref].append(card)

      send_email(email, 'sumup', available_cards_watched_by_email, unavailable_cards_watched_by_email)

def check():
  json_api = json.load(open('/app/config.json'))
  cards    = get_cards_from_retailers()

  if len(cards['available']) > 0:
    for email in json_api['email_list']:

      # To save each cards wanted for current email
      available_cards_watched_to_buy = {}
      for card_ref in json_api['email_list'][email]:

        if card_ref in cards['available']:

          for card in cards['available'][card_ref]:

            if card['product_name'].find(card_ref) > 0:
              head, separator, tail = card['product_price'].partition('€')
              if int(head.strip().replace(' ', '')) <= int(json_api['email_list'][email][card_ref]['max_price']):
                if card_ref not in available_cards_watched_to_buy:
                  available_cards_watched_to_buy[card_ref] = [card]
                else:
                  available_cards_watched_to_buy[card_ref].append(card)

      # Send warning email if price is ok
      if len(available_cards_watched_to_buy) > 0:
        send_email(email, 'check', available_cards_watched_to_buy)

def send_email(email_to, email_type, cards_available_list = {}, cards_unavailable_list = {}):
  # Get SMTP configuration
  json_file   = open('/app/config.json')
  json_config = json.load(json_file)
  
  # Init email to send card list to email_to 
  email = Email(json_config['email_server']['smtp_server_url'], json_config['email_server']['smtp_server_port'], json_config['email_server']['smtp_server_login'], json_config['email_server']['smtp_server_password'])
  email.set_email_from(json_config['email_server']['smtp_server_login'])
  email.set_email_to(email_to)
  
  if 'sumup' == email_type:
    email.set_email_subject("Today's graphics cards prices") 
    email.set_text_message("Today's graphics cards prices.")
    picture_top_path = '/app/picture/email_sumup_top_picture.png'
    template         = codecs.open('/app/template/email/product_sumup.html', 'r')
    email.set_html_message(template.read(), {"cards_available_list": cards_available_list, "cards_unavailable_list": cards_unavailable_list})
  
  if 'check' == email_type and len(cards_available_list) > 0:
    email.set_email_subject("Cards that you can buy right now bitch !")
    email.set_text_message("Cards that you can buy right now bitch !")
    picture_top_path = '/app/picture/email_check_top_picture.png'
    template         = codecs.open('/app/template/email/product_check.html', 'r')
    email.set_html_message(template.read(), cards_available_list)

  email.set_email_html_picture([{"picture_cid": "email_top_picture", "picture_path": picture_top_path}])
  email.send()

def log(name, start_or_end = 'started'):
  text_file = open("/var/log/crawler.log", "a+")
  text_file.write("{} {} at {}.\r\n".format(name.capitalize(), start_or_end, datetime.datetime.now().strftime('%b %d %Y %H:%M:%S')))
  if 'started' != start_or_end:
    text_file.write("-----------------------------------------------\r\n")
  text_file.close()

""" Main function launch on scrip bootstrap."""
if __name__ == "__main__": 
  argv   = sys.argv[1:]
  # Set default action to avoid error if option is not set with -a ou --action
  action = 'sumup'  
  try:
    opts, args = getopt.getopt(sys.argv[1:], "ho:v", ["help", "option="])
  except getopt.GetoptError:
    sys.exit(2)

  for opt, arg in opts:
    if opt in ("-h", "--help"):
      # Add help metho to dispay options available
      sys.exit() 
    elif opt in ("-o", "--option"):
      action = arg 
  
  log(action)
  if 'sumup' == action:
    sumup()
  elif 'check' == action:
    check()
  log(action, 'ended')
  sys.exit()

else:
  print ("Executed when imported")
