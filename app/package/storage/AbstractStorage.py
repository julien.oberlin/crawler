from abc import ABC, abstractmethod

class AbstractStorage(ABC):

  _connection = None

  def __init__(self, host_name: str, host_port: int, host_password: str, db_name=0):
    super().__init__()

  @abstractmethod
  def set(self, key: str, value):
    pass

  @abstractmethod
  def get(self, key: str):
    pass

  @abstractmethod
  def delete(self, key: str):
    pass
