import datetime
import redis
import json
from package.storage.AbstractStorage import AbstractStorage

"""
  To store data on redis cache
"""
class Redis(AbstractStorage):

  def __init__(self, host_name: str, host_port: int, host_password: str, db_name=0):
    try:
      self._connection = redis.StrictRedis(
        host=host_name,
        port=host_port,
        password=host_password,
        db=db_name)
      self._connection.ping()
    except Exception as ex:
      exit('Failed to connect, terminating.')

  def set(self, key: str, value):
    if isinstance(value, dict):
      value = json.dumps(value)
    self._connection.set(key, value)

  def get(self, key: str):
    value = self._connection.get(key)
    try:
      return json.loads(value)
    except:
      return value

  def delete(self, key: str):
    self._connection.delete(key)
