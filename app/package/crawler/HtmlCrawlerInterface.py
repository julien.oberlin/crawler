from package.crawler.HtmlCrawlerMeta import HtmlCrawlerMeta

class HtmlCrawlerInterface(metaclass=HtmlCrawlerMeta):
  
    def get(self, url: str, parameters={}) -> str:
        """Returns HTML or Json from URL."""
        pass
