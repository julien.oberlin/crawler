class HtmlCrawlerMeta(type):
  
    """A Connect metaclass that will be used for Connector class creation."""
    def __instancecheck__(cls, instance):
        return cls.__subclasscheck__(type(instance))

    def __subclasscheck__(cls, subclass):
        return (hasattr(subclass, 'get') and 
                callable(subclass.get))
