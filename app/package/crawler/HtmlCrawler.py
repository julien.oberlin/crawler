import requests
from bs4 import BeautifulSoup
from package.crawler.HtmlCrawlerInterface import HtmlCrawlerInterface

class HtmlCrawler(HtmlCrawlerInterface):
  
  """Headers to add to each query."""
  __headers = None
  
  def __init__(self):
    # Make our scraper looks like a legitimate browser
    self.__headers = {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Max-Age': '3600',
      'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'
    }

  """Returns GET query content 
  Keyword arguments:
  url        -- the url the GET
  parameters -- the parameters to add to GET query
  """
  def get(self, url: str, parameters={}) -> str:
    response = requests.get(url, params=parameters, headers=self.__headers)
    if (not response.ok): #If status_code == 200
      print(f'Code: {response.status_code}, url: {url}')
    elif ('application/json' == response.headers['content-type']): #If JSON returns get Json object.
      return response.json()
    else:
      return response.text

  def get_beautifulsoup_from_html(self, html: str):
    return BeautifulSoup(html, 'html.parser')
