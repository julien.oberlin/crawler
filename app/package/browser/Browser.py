from selenium import webdriver
from datetime import datetime
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from package.browser.BrowserInterface import BrowserInterface

class Browser(BrowserInterface):

    """Current web driver (Selenium, PhantomJs, ...)."""
    __web_driver                = None
    __page_screenshot_file_path = None

    """Init web driver with default options."""
    def __init__(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--start-maximized')
        self.__web_driver = webdriver.Chrome(options=chrome_options)

    """Returns current instance driver set."""
    def get_web_driver(self):
      return self.__web_driver

    """Close current driver instance."""
    def close_web_driver(self) -> None:
      self.__web_driver.close()

    """Returns HTML page source content from given url."""
    def get_page_source(self, url: str) -> str:
      self.__web_driver.get(url)
      return self.__web_driver.page_source

    """Take a full size page screenshot and save it."""
    def get_page_screenshot(self, url: str, file_name: str) -> str:
      screenshot_file_path = '/var/log/' + str(file_name) + '.png'
      # Get current window original size
      original_size        = self.__web_driver.get_window_size()
      # Get current window width with the whole scroll width
      required_width       = self.__web_driver.execute_script('return document.body.parentNode.scrollWidth')
      # Get current window height with the whole scroll height
      required_height      = self.__web_driver.execute_script('return document.body.parentNode.scrollHeight')
      # Resize the current window with new size
      self.__web_driver.set_window_size(required_width, required_height)
      # Avoids scrollbar
      self.__web_driver.find_element_by_tag_name('body').screenshot(screenshot_file_path)
      # Reset to original size
      self.__web_driver.set_window_size(original_size['width'], original_size['height'])

      return screenshot_file_path

    """Returns page screen shot file path."""
    def get_page_screenshot_file_path(self) -> str:
      return self.__page_screenshot_file_path

    """Fill in form with data and submit it"""
    def fill_form_and_submit(self, url: str, input_values_by_ids: dict, xpath_string: str, screenshot = False) -> str:
      # Save current page url
      current_url = self.__web_driver.current_url

      # Reach page with input fill in
      self.__web_driver.get(url)
      
      for input_value_by_id in input_values_by_ids:
        # Find input then fill in with input value
        #print(url + ' : ' + input_value_by_id['value'])
        self.__web_driver.find_element_by_id(input_value_by_id['id']).send_keys(input_value_by_id['value'])

      # Submit form
      self.__web_driver.find_element_by_xpath(xpath_string).click()
      
      # wait for URL to load with x milliseconds wait
      WebDriverWait(self.__web_driver, 500).until(EC.url_changes(current_url))

      if screenshot:
        # print new URL
        new_url = self.__web_driver.current_url
        self.__page_screenshot_file_path = self.get_page_screenshot(new_url, datetime.now().strftime("%Y.%d.%m_%H.%M.%S"))
      
      return self.__web_driver.page_source
