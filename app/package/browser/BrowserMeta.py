class BrowserMeta(type):
  
    """A Connect metaclass that will be used for Browser class creation."""
    def __instancecheck__(cls, instance):
        return cls.__subclasscheck__(type(instance))

    def __subclasscheck__(cls, subclass):
        return (hasattr(subclass, 'get_web_driver') and 
                callable(subclass.get_web_driver) and
                hasattr(subclass, 'close_web_driver') and
                callable(subclass.close_web_driver))
