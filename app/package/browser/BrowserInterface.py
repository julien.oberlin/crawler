from package.browser.BrowserMeta import BrowserMeta

class BrowserInterface(metaclass=BrowserMeta):

    def get_web_driver(self):
      """Return current driver instance."""
      pass

    def close_web_driver(self) -> None:
      """Close current driver instance."""
      pass
