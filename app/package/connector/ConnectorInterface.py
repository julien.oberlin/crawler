from package.connector.ConnectorMeta import ConnectorMeta
from package.browser.BrowserInterface import BrowserInterface

class ConnectorInterface(metaclass=ConnectorMeta):
  
    def fill_in_login_form(self, browser: BrowserInterface, login_form_url: str, login_input_id: str, password_input_id: str) -> None:
        """Go to URL login then fill in the login form."""
        pass

    def submit_login_form(self, xpath_string: str) -> None:
        """Submit login form with xpath query string."""
        pass
