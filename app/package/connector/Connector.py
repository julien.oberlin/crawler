from package.connector.ConnectorInterface import ConnectorInterface
from package.browser.BrowserInterface import BrowserInterface

class Connector(ConnectorInterface):

    """Website to log login/username."""
    __login       = None
    """Website to log password."""
    __password    = None
    """Current web driver."""
    __web_driver  = None

    """Connector constructor."""
    def __init__(self, browser: BrowserInterface, login: str, password: str):
        self.__login       = login
        self.__password    = password
        self.__web_driver  = browser.get_web_driver()

    """Go to URL login then fill in the login form."""
    def fill_in_login_form(self, login_form_url: str, login_input_id: str, password_input_id: str) -> None:
        """Reach login page"""
        self.__web_driver.get(login_form_url)
        """Find form login input then fill in with login string"""
        self.__web_driver.find_element_by_id(login_input_id).send_keys(self.__login)
        """Find form password input then fill in with password string"""
        self.__web_driver.find_element_by_id(password_input_id).send_keys(self.__password)

    """Submit login form with xpath query string."""
    def submit_login_form(self, xpath_string: str) -> None:
        self.__web_driver.find_element_by_xpath(xpath_string).click()
