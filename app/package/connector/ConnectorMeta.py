class ConnectorMeta(type):
  
    """A Connect metaclass that will be used for Connector class creation."""
    def __instancecheck__(cls, instance):
        return cls.__subclasscheck__(type(instance))

    def __subclasscheck__(cls, subclass):
        return (hasattr(subclass, 'fill_in_login_form') and 
                callable(subclass.fill_in_login_form) and
                hasattr(subclass, 'submit_login_form') and
                callable(subclass.submit_login_form))
