from abc import ABC, abstractmethod

class AbstractMessage(ABC):

  def __init__(self):
    super().__init__()

  @abstractmethod
  def send(self):
    pass

  @abstractmethod
  def set_text_message(self, text_message: str):
    pass
