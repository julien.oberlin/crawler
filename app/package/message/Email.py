import datetime
import smtplib
from jinja2 import Template, Environment, BaseLoader
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from package.message.AbstractMessage import AbstractMessage

"""
  To send email with picture, you have to set <img src="cid:image_name">.
  The image_name has to be equals to email_html_picture_cid set with
  set_email_html_picture() method
"""
class Email(AbstractMessage):

  """Email parameters """
  email_from            = None
  email_to              = None
  email_subject         = None
  email_text_message    = None
  email_msg_alternative = None
  __msgRoot             = None

  """SMTP server parameters."""
  __server_login        = None
  __server_password     = None
  __server_smtp         = None

  def __init__(self, server_url: str, server_port: int, server_login: str, server_password: str):
    self.now                = datetime.datetime.now()
    self.email_to           = []
    self.__msgRoot          = MIMEMultipart('related')
    self.__msgRoot.preamble = 'This is a multi-part message in MIME format.'
    try:
      self.__server_smtp  = smtplib.SMTP(server_url, server_port)
      self.__server_smtp.ehlo()
      self.__server_smtp.starttls()
      self.__server_smtp.login(server_login, server_password)
    except:
        print('Something went wrong on SMTP login ...')

  """Set sender email address."""
  def set_email_from(self, email_from: str):
    self.email_from        = email_from
    self.__msgRoot['From'] = self.email_from

  """Set email(s) address(es) to send send email."""
  def set_email_to(self, email_to):
    if type(email_to) is list:
      self.email_to = list(set(self.email_to + email_to))
    elif type(email_to) is str and email_to not in self.email_to:
      self.email_to.append(email_to)
    self.__msgRoot['To'] = ", ".join(self.email_to)

  """Set email subject."""
  def set_email_subject(self, email_subject):
    self.email_subject        = email_subject + ' %s' % self.now.strftime('%b %d %Y %H:%M:%S')
    self.__msgRoot['Subject'] = self.email_subject

  """Set email message text format."""
  def set_text_message(self, text_message: str):
    self.email_text_message    = text_message
    self.email_msg_alternative = MIMEMultipart('alternative')
    self.__msgRoot.attach(self.email_msg_alternative)
    self.email_msg_alternative.attach(MIMEText('This is the alternative plain text message.'))

  """Set email message html format."""
  def set_html_message(self, html_message: str, html_parameters: dict):
    template_html            = Template(html_message)
    template_html_rendering  = template_html.render(html_parameters = html_parameters)
    msgHtml                  = MIMEText(template_html_rendering, 'html')
    self.email_msg_alternative.attach(msgHtml)

  """
  Add image to email from picture path.
  picture_path_by_cid = [{"picture_path": "/path/image.png", "picture_cid": "unique_id_picture"}]
  """
  def set_email_html_picture(self, pictures_path_by_cid: dict):
    for picture_path_by_cid in pictures_path_by_cid:
      open_file   = open(picture_path_by_cid['picture_path'], 'rb')
      email_image = MIMEImage(open_file.read())
      open_file.close()
      email_image.add_header('Content-ID', '<' + picture_path_by_cid['picture_cid'] + '>')
      self.__msgRoot.attach(email_image)

  """Set server smtp login."""
  def set_server_smtp_login(self, server_login: str):
    self.__server_login = server_login

  """Set server smtp password."""
  def set_server_smtp_password(self, server_password: str):
    self.__server_password = server_password

  """Send email"""
  def send(self):
    self.__server_smtp.sendmail(self.email_from, self.email_to, self.__msgRoot.as_string())
    self.__server_smtp.quit()
