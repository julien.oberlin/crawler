import unicodedata
from bs4 import BeautifulSoup
from package.retailer.AbstractRetailer import AbstractRetailer

class LdlcRetailer(AbstractRetailer):

  def search_product(self, retailer_url: str, html: str):
    soup_products = super().get_crawler().get_beautifulsoup_from_html(html)
    products_html = soup_products.find_all("li", {"class": "pdt-item"})
    
    for product_html in products_html:
      
      product_name_tag        = product_html.find("div", {"class": "pdt-desc"})
      product_name            = product_name_tag.find("h3" , {"class": "title-3"}).find("a")
      product_name            = product_name.text if (product_name is not None) else ''
      product_url_tag         = product_name_tag.find("a")
      product_url_tag['href'] = product_url_tag['href'].replace(product_url_tag['href'], retailer_url + product_url_tag['href'])
      product_url             = product_url_tag['href']

      product_bag_tag         = product_html.find("div", {"class": "basket"})

      product_price           = product_bag_tag.find("div" , {"class": "price"}).find("div" , {"class": "price"})
      product_price           = product_price.text if (product_price is not None) else ''

      product_picture_tag     = product_html.find('div', {'class': 'pic'})
      product_picture         = product_picture_tag.find('img')
      product_picture_url     = product_picture['src']

      product_not_available   = product_bag_tag.find('a' , {'class': 'add-to-cart'})
      product_availability    = 'indisponible' if 'disabled' in product_not_available['class'] else 'disponible'
      product_json            = {"product_name": unicodedata.normalize("NFKD", product_name), "product_picture_url": product_picture_url, "product_url": product_url, "product_price": unicodedata.normalize("NFKD", product_price), "product_status": product_availability, "retailer_name": "ldlc"}

      if 'disponible' == product_availability:
        super().add_product_available_to_list(product_json)
      else:
        super().add_product_unavailable_to_list(product_json)
