from abc import ABC, abstractmethod
from package.crawler.HtmlCrawler import HtmlCrawler

class AbstractRetailer(ABC):

  """HTML Crawler (beautifulsoup,...) """
  __html_crawler = None

  """List to add available products from retailer."""
  __products_available_list = None

  """List to add uavailable products from retailer."""
  __products_unavailable_list = None

  def __init__(self):
    self.__products_available_list   = []
    self.__products_unavailable_list = []
    self.__html_crawler              = HtmlCrawler()
    super().__init__()

  @abstractmethod
  def search_product(self, retailer_url: str, html: str):
    pass

  """Set __products_available_list."""
  def set_product_available_list(self, products: dict):
    self.__products_available_list = products

  """Add product dictionary to array."""
  def add_product_available_to_list(self, products: dict):
    self.__products_available_list.append(products)

  """Returns product available dictionary."""
  def get_products_available_list(self) -> dict:
    return self.__products_available_list

  """Set __products_unavailable_list."""
  def set_product_unavailable_list(self, products: dict):
    self.__products_unavailable_list = products

  """Add product dictionary to array."""
  def add_product_unavailable_to_list(self, products: dict):
    self.__products_unavailable_list.append(products)

  """Returns product unavailable dictionary."""
  def get_products_unavailable_list(self) -> dict:
    return self.__products_unavailable_list

  """Returns current crawler."""
  def get_crawler(self) -> object:
    return self.__html_crawler
