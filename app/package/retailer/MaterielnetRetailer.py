import unicodedata
from bs4 import BeautifulSoup
from package.retailer.AbstractRetailer import AbstractRetailer

class MaterielnetRetailer(AbstractRetailer):

  def search_product(self, retailer_url: str, html: str):
    soup_products = super().get_crawler().get_beautifulsoup_from_html(html)
    products_html = soup_products.find_all("li", {"class": "ajax-product-item"})

    for product_html in products_html:

      product_name_tag        = product_html.find('div', {'class': 'c-product__meta'})
      product_name            = product_name_tag.find('h2', {'class': 'c-product__title'})        
      product_name            = product_name.text if (product_name is not None) else ''
      product_url_tag         = product_name_tag.find('a', {'class': 'c-product__link'})
      product_url_tag['href'] = product_url_tag['href'].replace(product_url_tag['href'], retailer_url + product_url_tag['href'])
      product_url             = product_url_tag['href']

      product_price_tag       = product_html.find('div', {'class': 'c-product__prices'})
      product_price           = product_price_tag.find('span' , {'class': 'o-product__price'})
      product_price           = product_price.text if (product_price is not None) else ''
                              
      product_picture_tag     = product_html.find('div', {'class': 'c-product__thumb'})
      product_picture         = product_picture_tag.find('img')
      product_picture_url     = product_picture['src']

      product_not_available   = product_price_tag.find('div' , {'class': 'c-product__button'}).find('button', 'o-btn__add-to-cart').has_attr('disabled')
   
      product_availability    = "indisponible" if (product_not_available) else "disponible"
      product_json            = {"product_name": unicodedata.normalize("NFKD", product_name), "product_picture_url": product_picture_url, "product_url": product_url, "product_price": unicodedata.normalize("NFKD", product_price), "product_status": product_availability, "retailer_name": "materiel_net"}

      if 'disponible' == product_availability:
        super().add_product_available_to_list(product_json)
      else:
        super().add_product_unavailable_to_list(product_json)
