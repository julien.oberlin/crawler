# crawler

![Web Crawler](icon.png)

Python script to login and crawl NVidia and other cards retailers web site.

### Install docker

1. Update system

First of all, update/upgrade your distribution (Debian for this e.g).
```shell
user@my_computer:~ $ sudo apt-get update && sudo apt-get -y upgrade # answer 'y' to upcoming questions 
```

2. Install Docker client

Install the Docker client on system with just one terminal command.
```shell
user@my_computer:~ $ curl -sSL https://get.docker.com | sh
```

3. Check installed version

If everything goes well, you should be able to check Docker version in console.
```shell
user@my_computer:~ $ docker --version
Docker version 19.03.12, build 48a66213fe
```

4. Avoid using docker with root

Adding a user to the "docker" group will grant the ability to run containers which can be used to obtain root privileges on the docker host.If you would like to use Docker as a non-root user, you should now consider adding your user to the "docker" group.
```shell
user@my_computer:~ $ sudo usermod -aG docker $USER
```

### Install docker-compose

1. Run this command to download the current stable release of Docker Compose.

To install a different version of Compose, substitute 1.29.1 with the version of Compose you want to use.
```shell
user@my_computer:~ $ sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

2. Apply executable permissions to the binary:

If the command docker-compose fails after installation, check your path. You can also create a symbolic link to /usr/bin or any other directory in your path.
```shell
user@my_computer:~ $ sudo chmod +x /usr/local/bin/docker-compose
```

3. Check installed version

If everything goes well, you should be able to check docker-composer version in console.
```shell
user@my_computer:~ $ docker-compose --version
docker-compose version 1.29.1, build 1110ad01
```

### Configure Crawler

1. config.json

Create config.json from sample file.
```shell
user@my_computer:~ $ cp app/config.sample.json app/config.json
```

First you have to configure the account the crawler will use to send email notification.
The exemple shows a configuration with a Gmail account.
If you want to use a Gmail email account, this app use TLS SMTP port 587.
Search on Google how to use SMTP server for Gmail or whatever you want to use with the crawler. 
```json
{
  "email": {
    "smtp_server_url": "smtp.gmail.com",
    "smtp_server_port": "587", 
    "smtp_server_login": "my.gmail.adress@gmail.com", 
    "smtp_server_password": "my_super_Strong_password"
  }
}
```

Next part of the config.json will set for each email address, wich graphic card GPU reference to watch and the max price the user want to buy the card.
This part will be used to filter the product to send the day sumup and also the card price to watch.
If a card price is equal or lower to the max_price defined, an email will be sent to user to warn that the card is available to buy. 
```json
{
    "email_list": {
        "email_to_send_to@domain.com": {
          "3090": {
            "max_price": "1600"
          },
          "6900": {
            "max_price": "1100"
          }
        }
    }
}
```

2. reailers.json

This json will help to browse search result page for a retailer.
The crawler uses this json to navigate on the HTML page to grab the data for each graphic card searched.
You have to set your account login and password, this part is here for a future feature that will trigger the buy process. 
```json
{
  "https://www.ldlc.com": {
    "retailer_name": "ldlc_com",
    "retailer_search_url": "https://www.ldlc.com/informatique/pieces-informatique/carte-graphique-interne/c4684/",
    "retailer_search_input_id": "filter_searchText",
    "retailer_search_button_xpath": "//input[@id='filter_searchText']/following-sibling::button",
    "sign_in_url": "https://secure2.ldlc.com/fr-fr/Login/Login",
    "sign_in_form_login_id": "Email",
    "sign_in_form_password_id": "Password",
    "sign_in_form_button_xpath": "//a[@id='partialLostPasswordLink']/following-sibling::button",
    "login": "#################_ldlc_login_#################",
    "password": "#################_ldlc_password_#################",
    "screenshot_url": "https://secure2.ldlc.com/fr-fr/Account/InformationsSection"
  }
}
```

### Start the Crawler
To start the python crawler you have to browse to this repo root directory to be at the same level of docker-compose.yml file.
```shell
user@my_computer:~ $ docker-compose build --no-cache && docker-compose up --build -d
```

### How it works

The crawler works with two cron. The first one send a daily sumup email with all available and unavailable cards. The second one is triggered every 15 minutes to check if a card is available for each email and for the max_price defined.
You can see this into the crontab.txt file. The first cron is commented, it's a test one that feed a file in order to be sure that the crontab works fine.
```shell
# Just feed a log file with message to test crontab
#*/1 * * * * python /app/test.py

# Check if product is available to buy and send email if it's 
*/15 * * * * python /app/main.py -o check

# Get all available and unavailable products and send sumup email
0 6 * * * python /app/main.py -o sumup
```
Each time an action is triggered, the log file crawler.log is filled within the log directory.

### Stop and clean the containers (Python and Redis)

Be careful with this command below, it will stop remove and purge every docker container started on your system.
```shell
user@my_computer:~ $ docker stop $(docker ps -a -q) || true && docker rm $(docker ps -a -q) || true && docker system prune -a --force --volumes && docker images purge
```
