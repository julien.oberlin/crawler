FROM python:3.9-alpine

### Create directory and copy files that we need
RUN mkdir /app
COPY app/ /app/

### Apply rights on app directory
RUN chmod -R 755 /app
RUN chown -R 1000:1000 /var/log

### Set this directory working directory
WORKDIR /app/

### To avoid "unsatisfiable constraints error" because the package you need may not
### be available in the main repository but in the the testing or community repositories.
RUN echo "http://dl-4.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories

### Add Supervisor
RUN apk --update add --no-cache \
        dcron \
        supervisor
        
RUN apk --update add --no-cache \
        chromium \
        chromium-chromedriver

ENV CHROME_BIN=/usr/bin/chromium-browser \
    CHROME_PATH=/usr/lib/chromium/

### Set Time zone
RUN apk add --no-cache --update tzdata
ENV TZ=Europe/Paris

### Clean APK cache (not realy usefull)
### https://www.sandtable.com/reduce-docker-image-sizes-using-alpine/
RUN rm -rf /var/cache/apk/*

### Install python stuff needed
RUN pip install --no-cache-dir -r requirements.txt

### Add all cron to schedule then run crontab file
COPY crontab.txt /etc/crontabs/root
#RUN cat /crontab.txt > /etc/crontabs/root

### Add supervisor config
COPY supervisord.conf /etc/

### Launch supervisord
CMD ["supervisord", "-n", "-c", "/etc/supervisord.conf"]
